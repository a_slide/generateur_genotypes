generateur_genotype
===================

Entrée = taille de génotype/haplotype + nombre d'haplotype + nombre de génotype + nombre d’ambiguïté max/ genotype Sortie = fichier contenant une liste de génotype + fichier "solution" contenant la même liste de génotype, mais avec les haplotypes à l'origine intercalés
